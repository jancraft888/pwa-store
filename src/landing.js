import { download, Profile, fetchIcon, AppleUUID, VERSION, dynImport } from './util.js'

// in case network fails
let source = { id: 'app.pwastore.core' }
let pwa = {
  id: 'storefront',
  name: 'PWA Store',
  icon: '/thirdparty/pwastore.png',
  url: 'https://pwastore.ainara.dev/',
  description: 'The iOS App Store for PWAs. Unleash the full power of web applications by installing them directly to your device with a single tap.'
}

const load = async () => {
  const src = await (await fetch('/core.json')).json()
  if (src) {
    source = src
    const srcpwa = src.pwas.find(x => x.id == 'storefront')
    if (srcpwa)
      pwa = srcpwa
  }
}
load()

async function buildAppProfile(source, pwa) {
  const p = new Profile(source.id + '.' + pwa.id, pwa.name, pwa.description, 1, `PWA Store v${VERSION}`, AppleUUID())
  p.webClip(pwa.name, pwa.url, pwa.icon ? await fetchIcon(pwa.icon) : null, 1, AppleUUID())

  return p
}

const getLocalMaterial = (s, n) => {
  let mat, hexmat
  if ((hexmat = localStorage.getItem(n))) {
    mat = Uint8Array.from(hexmat.match(/.{1,2}/g).map(byte => parseInt(byte, 16)))
  } else {
    mat = crypto.getRandomValues(new Uint8Array(s))
    hexmat = new Array(...mat).map(x => x.toString(16).padStart(2, '0')).join('')
    // fix ???
    mat = Uint8Array.from(hexmat.match(/.{1,2}/g).map(byte => parseInt(byte, 16)))
    localStorage.setItem(n, hexmat)
  }
  return [mat, hexmat]
}

let installing = false
const pwainstall = async () => {
  if (installing)
    return

  installing = true

  if (typeof BigInt !== 'function') {
    buildAppProfile(source, pwa).then(profile => {
      installing = false
      document.querySelectorAll('.tg-install').forEach(e => e.classList.remove('install-spin'))
      download(pwa.id + '.pwastore.mobileconfig', URL.createObjectURL(profile.blob()))
    })
    return
  }


  const { signProfile } = await import('./cert.js')

  document.querySelectorAll('.tg-install').forEach(e => e.classList.add('install-spin'))
  await new Promise(res => setTimeout(res, 400))

  let [ keymat, hexkeymat ] = getLocalMaterial(32, 'pwastore_keymat') // 256 bits for P-256
  pwa.url += `?k=${hexkeymat}`

  let [ serial, hexserial ] = getLocalMaterial(20, 'pwastore_serial') // 20 byte serial
  pwa.url += `&s=${hexserial}`

  let expiry, strexpiry
  if ((strexpiry = localStorage.getItem('pwastore_expiry'))) {
    expiry = parseInt(strexpiry)
  } else {
    expiry = Date.now()
    strexpiry = expiry.toFixed(0)
    localStorage.setItem('pwastore_expiry', strexpiry)
  }
  pwa.url += `&e=${strexpiry}`

  await new Promise(res => setTimeout(res, 400))

  //download(pwa.id + '.pwastore.mobileconfig', (await buildAppProfile(source, pwa)).url())
  signProfile(await buildAppProfile(source, pwa), keymat, serial, expiry, true).then(blob => {
    installing = false
    document.querySelectorAll('.tg-install').forEach(e => e.classList.remove('install-spin'))
    download(pwa.id + '.pwastore.mobileconfig', URL.createObjectURL(blob))
  })
}

window.addEventListener('load', () => {
  for (const e of document.querySelectorAll('.tg-install'))
    e.addEventListener('click', pwainstall)
})

