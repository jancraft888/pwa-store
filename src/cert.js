import { sha256 } from '@noble/hashes/sha256'
import { sha1 } from '@noble/hashes/sha1'
import { p256 } from '@noble/curves/p256'
import * as asn1js from 'asn1js'

function encodePEM(der) {
  return `-----BEGIN CERTIFICATE-----\n${btoa(String.fromCharCode.apply(null, der)).match(/.{1,64}/g).join('\n')}\n-----END CERTIFICATE-----`
}

function encodeASN1TBS(cert) {
  const makeName = data => new asn1js.Sequence({
    value: [
      new asn1js.Set({
        value: [
          new asn1js.Sequence({
            value: [
              new asn1js.ObjectIdentifier({ value: '2.5.4.6' }),
              new asn1js.PrintableString({ value: data.C })
            ]
          })
        ]
      }),
      new asn1js.Set({
        value: [
          new asn1js.Sequence({
            value: [
              new asn1js.ObjectIdentifier({ value: '2.5.4.10' }),
              new asn1js.PrintableString({ value: data.O })
            ]
          })
        ]
      }),
      new asn1js.Set({
        value: [
          new asn1js.Sequence({
            value: [
              new asn1js.ObjectIdentifier({ value: '2.5.4.3' }),
              new asn1js.PrintableString({ value: data.CN })
            ]
          })
        ]
      }),
    ]
  })

  const keysha1 = sha1(cert.data.spki.pub)

  return new asn1js.Sequence({
    value: [
      new asn1js.Constructed({
        idBlock: { tagClass: 3, tagNumber: 0 },
        value: [
          new asn1js.Integer({ value: cert.data.version })
        ]
      }),
      new asn1js.Integer({ valueHex: cert.data.serialNumber }),
      new asn1js.Sequence({
        value: [
          new asn1js.ObjectIdentifier({ value: cert.data.sigAlg }),
          new asn1js.Null()
        ]
      }),
      makeName(cert.data.issuer),
      new asn1js.Sequence({ // validity
        value: [
          new asn1js.UTCTime({ valueDate: cert.data.validity.nbf }),
          new asn1js.UTCTime({ valueDate: cert.data.validity.naf })
        ]
      }),
      makeName(cert.data.subject),
      new asn1js.Sequence({ // spki
        value: [
          new asn1js.Sequence({
            value: [
              new asn1js.ObjectIdentifier({ value: cert.data.spki.alg }),
              new asn1js.ObjectIdentifier({ value: cert.data.spki.oid })
            ]
          }),
          new asn1js.BitString({ valueHex: cert.data.spki.pub })
        ]
      }),
      new asn1js.Constructed({ // extensions
        idBlock: { tagClass: 3, tagNumber: 3 },
        value: [
          new asn1js.Sequence({
            value: [
              new asn1js.Sequence({
                value: [
                  new asn1js.ObjectIdentifier({ value: '2.5.29.14' }), // subject key id
                  new asn1js.OctetString({ valueHex: new Uint8Array([
                    0x04, 0x14,
                    ...keysha1
                  ]) })
                ]
              }),
              new asn1js.Sequence({
                value: [
                  new asn1js.ObjectIdentifier({ value: '2.5.29.35' }), // authority key id
                  new asn1js.OctetString({ valueHex: new Uint8Array([
                    0x30, 0x16, 0x80, 0x14,
                    ...keysha1
                  ]) })
                ]
              }),
              new asn1js.Sequence({
                value: [
                  new asn1js.ObjectIdentifier({ value: '2.5.29.19' }), // basic constraints
                  new asn1js.Boolean({ value: true }),
                  new asn1js.OctetString({
                    valueHex: new Uint8Array([ 0x30, 0x03, 0x01, 0x01, 0xFF ])
                  })
                ]
              }),
              new asn1js.Sequence({
                value: [
                  new asn1js.ObjectIdentifier({ value: '2.5.29.15' }), // key usage
                  new asn1js.Boolean({ value: true }),
                  new asn1js.OctetString({
                    valueHex: new Uint8Array([ 0x03, 0x02, 0x02, 0x84 ])
                  })
                ]
              })
            ]
          })
        ]
      })
    ]
  })
}

function encodeASN1Full(cert) {
  return new asn1js.Sequence({
    value: [
      encodeASN1TBS(cert),
      new asn1js.Sequence({
        value: [
          new asn1js.ObjectIdentifier({ value: cert.sigAlg }),
          new asn1js.Null()
        ]
      }),
      new asn1js.BitString({ valueHex: cert.sig })
    ]
  })
}

function createSelfSignedCertificate(keyMaterial, serial, expiry, name, country, organization, isCA) {
  // yea i have no idea which i should zero so i just do both
  serial[0] &= (0xFF ^ (1 << 7))
  serial[19] &= (0xFF ^ (1 << 7))
  const cert = {
    data: {
      version: 0x2,
      serialNumber: serial,
      sigAlg: '1.2.840.10045.4.3.2',
      issuer: {
        C: country,
        O: organization,
        CN: name
      },
      validity: {
        nbf: new Date(expiry),
        naf: new Date(expiry)
      },
      subject: { /* copy of issuer */ },
      spki: {
        alg: '1.2.840.10045.2.1',
        oid: '1.2.840.10045.3.1.7',
        pub: 0,
      },
      ext: { // unused lol
        basicConstraints: { ca: isCA },
        keyUsage: { digitalSignature: true, certificateSign: true }
      }
    },
    sigAlg: '1.2.840.10045.4.3.2',
    sig: 0
  }
  cert.data.subject = cert.data.issuer
  cert.data.validity.naf.setFullYear(cert.data.validity.nbf.getFullYear() + 10)

  cert._key = keyMaterial // use key material directly (32 bytes)
  cert._pub = p256.getPublicKey(cert._key, false) // uncompressed
  cert.data.spki.pub = cert._pub

  const tbs = encodeASN1TBS(cert).toBER()
  // this cost me more than 20 hours
  cert.sig = p256.sign(sha256(new Uint8Array(tbs)), cert._key).toDERRawBytes()

  return cert
}

function certificateToDER(cert) {
  const asn1 = encodeASN1Full(cert).toBER(false)
  return new Uint8Array(asn1)
}

function certificateToPEM(cert) {
  const asn1 = encodeASN1Full(cert).toBER(false)
  return encodePEM(new Uint8Array(asn1))
}

function signFileSMIME(data, cert) {
  const eContentOS = new asn1js.OctetString({ valueHex: data })
  const eContent = new asn1js.Constructed({
    idBlock: { tagClass: 3, tagNumber: 0 },
    value: [
      eContentOS
    ]
  })
  const encapContentInfo = new asn1js.Sequence({
    value: [
      new asn1js.ObjectIdentifier({ value: '1.2.840.113549.1.7.1' }),
      eContent,
    ]
  })

  /*
  const signedAttrs = [
    new asn1js.Sequence({ // content type
      value: [
        new asn1js.ObjectIdentifier({ value: '1.2.840.113549.1.9.3' }),
        new asn1js.Set({
          value: [
            new asn1js.ObjectIdentifier({ value: '1.2.840.113549.1.7.1' })
          ]
        })
      ]
    }),
    new asn1js.Sequence({ // signing time
      value: [
        new asn1js.ObjectIdentifier({ value: '1.2.840.113549.1.9.5' }),
        new asn1js.Set({
          value: [
            new asn1js.UTCTime({ valueDate: new Date() })
          ]
        })
      ]
    }),
    new asn1js.Sequence({ // message digest
      value: [
        new asn1js.ObjectIdentifier({ value: '1.2.840.113549.1.9.4' }),
        new asn1js.Set({
          value: [
            new asn1js.OctetString({
              valueHex: sha256(data)
            })
          ]
        })
      ]
    }),
    new asn1js.Sequence({ // sMIMECapabilities
      value: [
        new asn1js.ObjectIdentifier({ value: '1.2.840.113549.1.9.15' }),
        new asn1js.Set({
          value: [
            new asn1js.Sequence({
              value: [
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '2.16.840.1.101.3.4.1.42' })
                ] }),
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '2.16.840.1.101.3.4.1.22' })
                ] }),
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '2.16.840.1.101.3.4.1.2' })
                ] }),
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '1.2.840.113549.3.7 ' })
                ] }),
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '1.2.840.113549.3.2' })
                ] }),
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '1.2.840.113549.3.2' }),
                  new asn1js.Integer({ value: 64 })
                ] }),
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '1.3.14.3.2.7' })
                ] }),
                new asn1js.Sequence({ value: [
                  new asn1js.ObjectIdentifier({ value: '1.2.840.113549.3.2' }),
                  new asn1js.Integer({ value: 40 })
                ] }),
              ]
            })
          ]
        })
      ]
    })
  ]

  const signedAttributes = new asn1js.Constructed({ // signed attributes
    optional: true,
    idBlock: { tagClass: 3, tagNumber: 0 },
    value: signedAttrs
  })
  */

  // this took me literally 3+ hours
  //const saub = new Uint8Array(signedAttributes.toBER())
  //saub[0] = 0x31
  //const signature = p256.sign(sha256(saub), cert._key).toDERRawBytes()
  const signature = p256.sign(sha256(data), cert._key).toDERRawBytes()

  return new asn1js.Sequence({
    value: [
      new asn1js.ObjectIdentifier({ value: '1.2.840.113549.1.7.2' }),
      new asn1js.Constructed({
        idBlock: { tagClass: 3, tagNumber: 0 },
        value: [
          new asn1js.Sequence({
            value: [
              new asn1js.Integer({ value: 1 }),
              new asn1js.Set({
                value: [
                  new asn1js.Sequence({
                    value: [
                      new asn1js.ObjectIdentifier({ value: '2.16.840.1.101.3.4.2.1' }),
                      new asn1js.Null()
                    ]
                  }),
                ]
              }),
              encapContentInfo,
              new asn1js.Constructed({
                idBlock: { tagClass: 3, tagNumber: 0 },
                value: [
                  encodeASN1Full(cert)
                ]
              }),
              new asn1js.Set({
                value: [
                  new asn1js.Sequence({
                    value: [
                      new asn1js.Integer({ value: 1 }), // version
                      new asn1js.Sequence({ // sid
                        value: [
                          new asn1js.Sequence({ // issuer
                            value: [
                              new asn1js.Set({
                                value: [
                                  new asn1js.Sequence({
                                    value: [
                                      new asn1js.ObjectIdentifier({ value: '2.5.4.6' }),
                                      new asn1js.PrintableString({ value: cert.data.issuer.C })
                                    ]
                                  })
                                ]
                              }),
                              new asn1js.Set({
                                value: [
                                  new asn1js.Sequence({
                                    value: [
                                      new asn1js.ObjectIdentifier({ value: '2.5.4.10' }),
                                      new asn1js.PrintableString({ value: cert.data.issuer.O })
                                    ]
                                  })
                                ]
                              }),
                              new asn1js.Set({
                                value: [
                                  new asn1js.Sequence({
                                    value: [
                                      new asn1js.ObjectIdentifier({ value: '2.5.4.3' }),
                                      new asn1js.PrintableString({ value: cert.data.issuer.CN })
                                    ]
                                  })
                                ]
                              }),
                            ]
                          }),
                          new asn1js.Integer({ valueHex: cert.data.serialNumber })
                        ]
                      }),
                      new asn1js.Sequence({ // digest algorithm
                        value: [
                          new asn1js.ObjectIdentifier({ value: '2.16.840.1.101.3.4.2.1' }),
                          new asn1js.Null()
                        ]
                      }),
                      //signedAttributes,
                      new asn1js.Sequence({ // signature algorithm
                        value: [
                          new asn1js.ObjectIdentifier({ value: cert.sigAlg }),
                          new asn1js.Null()
                        ]
                      }),
                      new asn1js.OctetString({ valueHex: signature })
                    ]
                  })
                ]
              })
            ]
          })
        ]
      })
    ]
  }).toBER(false)
}

export async function signProfile(profile, keyMaterial, serialNumber, expiry, containCert=false) {
  // Generate CA certificate
  const ca = createSelfSignedCertificate(keyMaterial, serialNumber, expiry, 'PWA Store Signing Certificate', 'Your iDevice', 'PWA Store', true)

  // Print CA certificate to the console
  console.log('CA Certificate:')
  const pem = certificateToPEM(ca)
  console.log(pem)
  if (containCert) {
    //profile.certificatePEM('PWA Store Signing Certificate', btoa(pem), 1)
    profile.certificateRoot('PWA Store Signing Certificate', certificateToDER(ca), 1)
  }

  const smimeResultBin = signFileSMIME(new TextEncoder().encode(profile.string()), ca)
  return new Blob([ smimeResultBin ], { type: 'application/x-apple-aspen-config' })
}
