
# PWA Store
The iOS app store for PWAs.

## Installation
To install PWA Store, just open https://pwastore.ainara.dev/ and follow the popup to install it.

When you install a PWA, a profile will be downloaded. Go to `Settings App > General > VPN & Device Management` then click on the downloaded profile and install it. Profiles are generated on-device and only contain the Web Clip required to install the PWA.

## Android support
Currently, Android is only supported for PWAs that can be fully installed as standalone apps. To install them, just press the three dots and then press *Install app* or *Add to homescreen*.

## Thirdparty
This repo hosts some third-party icons. These icons are under their respective licenses and we use them under their guidelines.
**If your icon is being used in this repo and you want it gone, please open an issue or send us an [email](mailto:me@ainara.dev).**

## Buttons
After the v2.0 release, we're reworking buttons. Please check back later!
